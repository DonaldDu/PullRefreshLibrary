#PullRefreshLibrary
https://github.com/naver/android-pull-to-refresh->library
做项目时，经常需要使用下拉刷新，如果直接用打包后的aar文件，在预览界面总是报错，需要导入项目源码才能正常预览。
直接导入原项目很慢，所以从原项目中把library独立了出来，以后直接导入这个项目就可以了。

重命名 PullRefreshLibrary.iml2 -> PullRefreshLibrary.iml 就可以正常使用Grade了